let display = document.querySelector(".display");
let spanDisplay = document.querySelector(".display span");
let keys = document.querySelectorAll(".keys button");
let span2 = document.createElement("span");
let br = document.createElement("br");
// regex qui verifie si les signes - ou +, qui suivent un nombre, sont les derniers du calcul
let regexWithNumberFollowedByPlusOrMinusLikeEnd = /\d+(\.\d+)?[-+]$/;
// regex qui verifie si les signes x ou ÷, qui suivent un nombre, sont les derniers du calcul 
let regexWithNumberFollowedByMultipliedOrDividedLikeEnd = /\d+(\.\d+)?[x÷]$/;
// regex qui verifie si le signe -, qui suit les signes x ou ÷, est le dernier du calcul
let regexWithMultipliedOrDividedFollowedByMinusLikeEnd = /[x÷]-$/;



function calculation(e) {
    let key = e.target.textContent;
    let cutedCalculation;
    
    /*
        si le spanDisplay est vide,
        alors on regarde si la touche tapée est un chiffre ou le signe -,
        si c'est oui on ajoute cela à spanDisplay
    */
    if(spanDisplay.textContent === "") {
        if(/\d/.test(key) || key == "-") {
            spanDisplay.textContent = key;
        }
    } 
        // sinon si spanDisplay commence avec un signe - et que l'on a tapée un chiffre, on ajoute ce chiffre à spanDisplay
    else if(/^[-]/.test(spanDisplay.textContent) && /\d/.test(key)) {
        spanDisplay.textContent += key;
    }

    else {
        // si le dernier element de spanDisplay est un nombre et que la touche tapée est un nombre ou un des signes - + ÷ x ou un point, on ajoute 
        if(/\d+$/.test(spanDisplay.textContent) && /\d|[-+÷x\.]/.test(key)) {
            spanDisplay.textContent += key;
        }
        // si le dernier element de spanDisplay est un point et que la touche tapé est un chiffre, ajoute ce chiffre à spanDisplay
        if(/\.+$/.test(spanDisplay.textContent) && /\d/.test(key)) {
            spanDisplay.textContent += key;
        }

        // si  le dernier element de spanDisplay est + ou - et que la touche tapée est un des signes - + ÷ x on remplace par le signe tapé
        else if(regexWithNumberFollowedByPlusOrMinusLikeEnd.test(spanDisplay.textContent) && /[-+÷x]/.test(key)) {
            cutedCalculation = spanDisplay.textContent.slice(0, -1);
            spanDisplay.textContent = cutedCalculation + key;
        }
        // si  le dernier element de spanDisplay est + ou - et que la touche tapée est un nombre, on ajoute
        else if(regexWithNumberFollowedByPlusOrMinusLikeEnd.test(spanDisplay.textContent) && /\d/.test(key) ) {
            spanDisplay.textContent += key;
        }
        // si le dernier element de spanDisplay est x ÷ et que la touche tapée est un nombre ou le signe -, on ajoute
        else if (regexWithNumberFollowedByMultipliedOrDividedLikeEnd.test(spanDisplay.textContent) && /\d|[-]/.test(key)) {
            spanDisplay.textContent += key;
        }
        // si le dernier element de spanDisplay est x ÷ et que la touche tapée est un des signe + ÷ x, on remplace par le signe tapé
        else if (regexWithNumberFollowedByMultipliedOrDividedLikeEnd.test(spanDisplay.textContent) && /[+÷x]/.test(key)) {
            cutedCalculation = spanDisplay.textContent.slice(0, -1);
            spanDisplay.textContent = cutedCalculation + key;
        }
        /*
            si le dernier element de spanDisplay est le signe -, 
            qui suit les signes x ou ÷, et que la touche tapée est un des signes + ÷ x,
            on remplace les signes ÷- ou x- par le signe tapé 
        */
        else if (regexWithMultipliedOrDividedFollowedByMinusLikeEnd.test(spanDisplay.textContent)  && /[+÷x]/.test(key)) {
            cutedCalculation = spanDisplay.textContent.slice(0, -2);
            spanDisplay.textContent = cutedCalculation + key;
        }

        else if (regexWithMultipliedOrDividedFollowedByMinusLikeEnd.test(spanDisplay.textContent)  && /\d/.test(key)) {
            spanDisplay.textContent += key;
        }
    }


    if (key === "C") {
        spanDisplay.parentNode.classList.remove("calculation-error")
        spanDisplay.textContent = "";
        // on verifie si dans la zone d'affichage on a deux elements span, si oui on supprime les deux derniers elements
        if(display.querySelectorAll("span").length ==2){
            display.removeChild(br);
            display.removeChild(span2);
        }

    }
    
    if (e.target.getAttribute("data-value") === "delete") {
        spanDisplay.parentNode.classList.remove("calculation-error");
        cutedCalculation = spanDisplay.textContent.slice(0, -1);
        spanDisplay.innerHTML = cutedCalculation;
        // on verifie si dans la zone d'affichage on a deux elements span, si oui on supprime les deux derniers elements
        if(display.querySelectorAll("span").length ==2){
            display.removeChild(br);
            display.removeChild(span2);
        }
    }

    if(key === "=") {
        if(/\d$/.test(spanDisplay.textContent)) {
            spanDisplay.parentNode.classList.remove("calculation-error");
            let resultat = spanDisplay.textContent;
            // on remplace tous les signes x et ÷ du calcul(s'ils existent) par * et /
            resultat = resultat.replace(/x/g, "*");
            resultat = resultat.replace(/÷/g, "/");
            span2.innerHTML = eval(resultat);
            // on insère le br comme dernier element avant d'inserer le span dans l'affichage
            display.appendChild(br);
            // on insère le span comme dernier element dans l'affichage
            display.appendChild(span2); 
        }
         
        else {
            spanDisplay.parentNode.classList.add("calculation-error");
        }
        
    }

}

keys.forEach((key) => key.addEventListener("click", calculation));